﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace png2blp {
    public class FileConverter {
        private readonly string _fileName;
        private readonly string _outFileName;
        private int[] _imageData;
        private int[] _fullImageData;
        private readonly List<int[]> _rgbaLayerData = new List<int[]>();
        private readonly List<byte[]> _layerData = new List<byte[]>();

        private int _width;
        private int _height;

        private int _fullWidth;
        private int _fullHeight;

        public FileConverter(string file) {
            _fileName = file;
            _outFileName = Path.ChangeExtension(_fileName, "blp");
        }

        public void convert() {
            parse();
            createPaddedFile();
            buildMipMaps();
            createBlpData();
            writeBlp();
        }

        private void parse() {
            using (var fileStream = File.OpenRead(_fileName)) {
                if (!(Image.FromStream(fileStream) is Bitmap image)) {
                    throw new ArgumentException("Invalid image file");
                }

                _width = image.Width;
                _height = image.Height;

                _fullWidth = nextPowerOfTwo(_width);
                _fullHeight = nextPowerOfTwo(_height);

                var data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadOnly,
                    PixelFormat.Format32bppArgb);

                _imageData = new int[image.Width * image.Height];
                Marshal.Copy(data.Scan0, _imageData, 0, _imageData.Length);
                image.UnlockBits(data);
            }
        }

        private void writeBlp() {
            using (var fileStream = File.Open(_outFileName, FileMode.OpenOrCreate)) {
                var writer = new BinaryWriter(fileStream);
                writer.Write(0x32504C42);
                writer.Write(0x00000001);
                writer.Write((byte) 2);
                writer.Write((byte) 0);
                writer.Write((byte) 7);
                writer.Write((byte) 0);
                writer.Write(_fullWidth);
                writer.Write(_fullHeight);
                var baseOffset = 148;
                for (var i = 0; i < 16; ++i) {
                    if (i < _layerData.Count) {
                        writer.Write(baseOffset);
                        baseOffset += _layerData[i].Length;
                    } else {
                        writer.Write((uint) 0);
                    }
                }

                for (var i = 0; i < 16; ++i) {
                    if (i < _layerData.Count) {
                        writer.Write(_layerData[i].Length);
                    } else {
                        writer.Write((uint) 0);
                    }
                }

                foreach (var layer in _layerData) {
                    writer.Write(layer);
                }
            }
        }

        private unsafe void createBlpData() {
            for (var i = 0; i < _rgbaLayerData.Count; ++i) {
                var width = Math.Max(1, _fullWidth >> i);
                var height = Math.Max(1, _fullHeight >> i);
                fixed (int* ptr = _rgbaLayerData[i]) {
                    var layerData = new byte[((width + 3) / 4) * ((height + 3) / 4) * 16];
                    fixed (byte* outPtr = layerData) {
                        LibTxc.CompressDxtn(width, height, (byte*) ptr, LibTxc.GlCompressedRgbaS3TcDxt5Ext, outPtr, 0);
                    }

                    _layerData.Add(layerData);
                }
            }
        }

        private void createPaddedFile() {
            if (_width == _fullWidth && _height == _fullHeight) {
                _fullImageData = _imageData;

                for (var y = 0; y < _fullHeight; ++y) {
                    for (var x = 0; x < _fullWidth; ++x) {
                        var current = _imageData[y * _width + x];
                        current = (int) (current & 0xFF00FF00) | ((current & 0xFF) << 16) | ((current >> 16) & 0xFF);
                        _fullImageData[y * _fullWidth + x] = current;
                    }
                }

                return;
            }

            _fullImageData = new int[_fullWidth * _fullHeight];
            for (var y = 0; y < _fullHeight; ++y) {
                for (var x = 0; x < _fullWidth; ++x) {
                    if (y < _height && x < _width) {
                        var current = _imageData[y * _width + x];
                        current = (int) (current & 0xFF00FF00) | ((current & 0xFF) << 16) | ((current >> 16) & 0xFF);
                        _fullImageData[y * _fullWidth + x] = current;
                    } else {
                        _fullImageData[y * _fullWidth + x] = 0;
                    }
                }
            }
        }

        private void buildMipMaps() {
            var curWidth = _fullWidth;
            var curHeight = _fullHeight;

            while (curWidth > 1 || curHeight > 1) {
                var pixelStepX = _fullWidth / curWidth;
                var pixelStepY = _fullHeight / curHeight;

                var layerData = new int[curWidth * curHeight];
                for (var y = 0; y < curHeight; ++y) {
                    for (var x = 0; x < curWidth; ++x) {
                        layerData[y * curWidth + x] = _fullImageData[y * pixelStepY * _fullWidth + x * pixelStepX];
                    }
                }

                _rgbaLayerData.Add(layerData);
                curWidth = Math.Max(1, curWidth >> 1);
                curHeight = Math.Max(1, curHeight >> 1);
            }
        }

        private static int nextPowerOfTwo(int value) {
            for (var i = 0; i < 32; ++i) {
                if (1 << i >= value) {
                    return 1 << i;
                }
            }

            throw new ArgumentException("Invalid power of two request");
        }
    }
}