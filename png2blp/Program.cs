﻿namespace png2blp {
    internal class Program {
        public static void Main(string[] args) {
            foreach (var file in args) {
                var converter = new FileConverter(file);
                converter.convert();
            }
        }
    }
}